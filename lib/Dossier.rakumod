unit class Dossier;

use YAMLish;
use JSON::Fast;
use MIME::Base64;
use Dossier::Transport::MockSock;

has Str  $!name    = 'dossier';
has Bool $.test    = False;
has Str  $.ipcpath = '/tmp/dossier.ipc';
has Str  $.cpath;
has Hash $.dossiers;

has $!ipcserver;
has $.dbobj;
has $.mockedsock = Dossier::Transport::MockSock.new;
has $!mimeobj    = MIME::Base64.new;

has $.oncreate_callback = sub (Dossier :$dossier!, Cool :$chatid! --> Hash) { Hash.new; };
has $.onupdate_callback = sub (Dossier :$dossier!, Cool :$chatid! --> Hash) { Hash.new; };

method set_ipcserver returns Bool {
    if $!ipcpath.IO.e {
        X::AdHoc.new(:payload(sprintf("IPC path <%s> is locked", $!ipcpath // q{}))).throw
            unless $!ipcpath.IO.unlink;
    }

    $!ipcserver = IO::Socket::INET.listen: $!ipcpath, 0, family => PF_UNIX;

    return True;
}

method get_name returns Str {
    return $!name;
}

method get_active_graphname(Cool :$chatid!) returns Str {
    return $!dossiers{$chatid}<activegraph> // 'dossier';
}

method activate_dossier(Cool :$chatid!, Str :$graphname = 'dossier') returns Bool {
    $!dossiers{$chatid}<activegraph> = $graphname;

    return True;
}

method reset_dossier(Cool :$chatid!) returns Bool {
    my $graphname = $!dossiers{$chatid}<activegraph> // 'dossier';
    my $next      = $!dossiers{$chatid}{$graphname}<next>;

    if $next.defined && $next == 0 {
        $!dossiers{$chatid}{$graphname}<next>:delete;
        $!dossiers{$chatid}{$graphname}<update> = True;

        return True;
    }

    return False;
}

method get_dossier(Cool :$chatid!, Str :$graphname) returns Hash {
    my $gn = $graphname // ($!dossiers{$chatid}<activegraph> // 'dossier');

    return $!dossiers{$chatid}{$gn} // {}
}

method load_config returns Hash {
    return load-yaml($!cpath.IO.slurp) // {};
}

method load_graph(Cool :$chatid!) returns List {
    my $graphname = $!dossiers{$chatid}<activegraph> // 'dossier';
    my $config    = self.load_config;

    return $config<chat>{$graphname} // [];
}

method debug {
    my $graphname = 'dossier';

    X::AdHoc.new(:payload(sprintf("config path <%s> is not found", $!cpath // q{}))).throw
        unless $!cpath && $!cpath.IO.e && $!cpath.IO.f;

    my $config = self.load_config;

    X::AdHoc.new(:payload('no chat found')).throw unless $config<chat> && $config<chat>.elems;

    my $graph = $config<chat>{$graphname};

    X::AdHoc.new(:payload('no chat graph found')).throw unless $graph && $graph.elems;

    self.set_ipcserver;

    X::AdHoc.new(:payload('no IPC server started')).throw unless $!ipcserver ~~ IO::Socket::INET;

    my $g_head = $graph.head;
    my $g_tail = $graph.tail;

    X::AdHoc.new(:payload('no start node')).throw unless $g_head<type> && $g_head<type> eq 'start';
    X::AdHoc.new(:payload('no end node')).throw   unless $g_tail<type> && $g_tail<type> eq 'finish';

    while (1) {
        my $accepted = self.client_request;

        next unless $accepted.keys && $accepted<sock> && $accepted<chatid>;

        self.activate_dossier(:chatid($accepted<chatid>), :$graphname);

        my $saved_dossier = self.get_dossier(:chatid($accepted<chatid>));

        $accepted<graphname> = $graphname;

        self.reach(:accepted($accepted), :graph($config<chat>), :target($saved_dossier<next> // 1));
    }
}

method reach(:$accepted!, List :$graph!, Int :$target!) {
    return unless $graph.elems;

    my $chatkey = $accepted<graphname> // 'dossier';

    if ($target == 0) {
        my $dossier_msg = to-json(
            {
                dossier => {
                    chatid   => $accepted<chatid>,
                    nodeid   => $target,
                    question => sprintf("dossier for chat %d:%s is collected", $accepted<chatid>, $chatkey),
                    toggles  => ['ok'],
                }
            }
        );

        $accepted<sock>.print(sprintf("%s\n", $dossier_msg));
        $accepted<sock>.close;

        return;
    }

    for $graph.values -> $node {
        if $node<id> == $target {
            $!dossiers{$accepted<chatid>}{$chatkey}<next> = $target;

            my @avail_toggles = self.available_toggles(:toggles($node<toggles> // []));

            if !($accepted<answer> && $accepted<nodeid>) {
                my $question = $node<question>;

                if $node<substitute> && $node<substitute><format> && $node<substitute><answerfromnode> {
                    my $answer = $!dossiers{$accepted<chatid>}{$chatkey}<nodes>{$node<substitute><answerfromnode>}<answer>;

                    if $answer {
                        my $s = sprintf($node<substitute><format>, $answer);

                        for $question.keys -> $key {
                            $question{$key} ~~ s:g/ '{substitute}' /$s/;
                        }
                    }
                }

                my $dossier_msg = {
                    dossier => {
                        chatid    => $accepted<chatid>,
                        graphname => $accepted<graphname>,
                        nodeid    => $node<id>,
                        question  => $question,
                        toggles   => @avail_toggles,
                        buttons   => $node<buttons> // [],
                        ($!dossiers{$accepted<chatid>}{$chatkey}<nodes>{$target}) ??
                            %(answer => $!dossiers{$accepted<chatid>}{$chatkey}<nodes>{$target}<answer>) !!
                                %()
                    }
                };

                my $dossier_json = to-json($dossier_msg);

                $accepted<sock>.print(sprintf("%s\n", $dossier_json));
                $accepted<sock>.close;

                $!dossiers{$accepted<chatid>}{$chatkey}<update> = False if $target == 1 && !$!dossiers{$accepted<chatid>}{$chatkey}<update>.defined;

                return $dossier_msg;
            }
            else {
                return unless $target == $accepted<nodeid>;

                my $received = $accepted<answer>;

                return unless $received.defined && $received ne q{};

                if $node<minchars> && $node<minchars> > 0 {
                    my ($toggle) = @avail_toggles.grep({ $_ eq $received });

                    if $received.chars > $node<minchars> || $toggle {
                        $!dossiers{$accepted<chatid>}{$chatkey}<next> =
                            self.next_toggles(:toggles($node<toggles> // []), :input($received)) || ($node<next> // 0);
                    }
                    else {
                        sprintf("***err at chat %d:%s node %d: answer minimum %d chars", $accepted<chatid>, $chatkey, $node<id>, $node<minchars>).say unless $!test;
                    }
                }
                else {
                    $!dossiers{$accepted<chatid>}{$chatkey}<next> =
                        self.next_toggles(:toggles($node<toggles> // []), :input($received)) || ($node<next> // 0);
                }

                if $target < $!dossiers{$accepted<chatid>}{$chatkey}<next> || $!dossiers{$accepted<chatid>}{$chatkey}<next> == 0 {
                    $!dossiers{$accepted<chatid>}{$chatkey}<nodes>{$target} = {
                        question => $node<question>,
                        answer   => ($!dossiers{$accepted<chatid>}{$chatkey}<update> && $!dossiers{$accepted<chatid>}{$chatkey}<nodes>{$target}<answer> && $accepted<answer> eq 'skip') ?? $!dossiers{$accepted<chatid>}{$chatkey}<nodes>{$target}<answer> !! $accepted<answer>
                    };

                    if $!dossiers{$accepted<chatid>}{$chatkey}<next> == 0 {
                        my $callback_exception;

                        try {
                            if $!dossiers{$accepted<chatid>}{$chatkey}<update> &&
                              ($!dossiers{$accepted<chatid>}{$chatkey}<data>:exists) &&
                              ($!dossiers{$accepted<chatid>}{$chatkey}<data><oncreate>:exists) &&
                               $!dossiers{$accepted<chatid>}{$chatkey}<data><oncreate> ~~ Hash &&
                               $!dossiers{$accepted<chatid>}{$chatkey}<data><oncreate>.keys.elems
                            {
                                $!dossiers{$accepted<chatid>}{$chatkey}<data><onupdate> = $!onupdate_callback(:dossier(self), :chatid($accepted<chatid>));
                            }
                            else {
                                $!dossiers{$accepted<chatid>}{$chatkey}<data><oncreate> = $!oncreate_callback(:dossier(self), :chatid($accepted<chatid>));
                            }

                            CATCH {
                                default {
                                    $callback_exception = .message;
                                }
                            }
                        }

                        my Bool $dbstatus = False;

                        if $!dbobj &&
                           $!dbobj.^find_method('get').defined &&
                           $!dbobj.^find_method('set').defined &&
                           $!dbobj.^find_method('insert').defined
                        {
                            my $dossier_record = $!dbobj.get({chatid => $accepted<chatid>});

                            if $dossier_record.keys {
                                my $record_set = {
                                    json => $!mimeobj.encode-str(to-json($!dossiers{$accepted<chatid>}), :oneline)
                                };

                                $dbstatus = $!dbobj.set($record_set, {chatid => $accepted<chatid>}).Bool;
                            }
                            else {
                                my $record_insert = {
                                    dossierid => time,
                                    chatid    => $accepted<chatid>,
                                    json      => $!mimeobj.encode-str(to-json($!dossiers{$accepted<chatid>}), :oneline),
                                };

                                $dbstatus = $!dbobj.insert($record_insert);
                            }
                        }

                        sprintf("dossier for chat %d:%s is collected\ncallback message: %s\ndatabase store status %s:\n%s", $accepted<chatid>, $chatkey, ($callback_exception ?? $callback_exception !! 'success'), $dbstatus, to-json($!dossiers{$accepted<chatid>})).say unless $!test;

                        $!dossiers{$accepted<chatid>}{$chatkey}<update> = False;
                    }
                }

                $accepted<sock>.close;
            }
        }
    }

    return;
}

method client_request returns Hash {
    my $accepted = $!ipcserver.accept;
    my $request  = $accepted.recv;

    my %json;

    return unless $request;

    try {
        %json = from-json($request);

        CATCH {
            default {
                my $e = 'can not parse JSON request ' ~ .^name ~ ': ' ~ .message;

                X::AdHoc.new(:payload($e)).throw;
            }
        }
    };

    if %json<get_dossier_question> && %json<chatid> {
        return {
            sock   => $accepted,
            chatid => %json<chatid>,
        };
    }

    if %json<dossier_answer> {
        return {
            sock   => $accepted,
            chatid => %json<chatid>,
            answer => %json<dossier_answer>,
            nodeid => %json<nodeid>,
        }
    }

    return {};
}

method available_toggles(List :$toggles) returns List {
    return [] unless $toggles && $toggles.elems;

    my @toggles_hint;

    for $toggles.values -> $toggle {
        @toggles_hint.push($toggle ~~ Hash ?? self.remap_toggle_name(:key($toggle.keys[0])) !! self.remap_toggle_name(:key($toggle)));
    }

    return @toggles_hint;
}

method next_toggles(List :$toggles, Str :$input) returns Int {
    return 0 unless $toggles && $toggles.elems;

    for $toggles.values -> $toggle {
        my $toggle_key = self.map_input(:in($input));

        if $toggle{$toggle_key} {
            return 0 unless $toggle{$toggle_key}.elems;

            my $next = ($toggle{$toggle_key}.head)<next>;

            return 0 unless $next;

            return $next;
        }
    }

    return 0;
}

method map_input(Str :$in = 'no') returns Cool {
    return $in if $in eq 'skip';

    return ($in eq 'yes') ?? True !! False;
}

method remap_toggle_name(Cool :$key = False) returns Str {
    return $key if $key ~~ Str && $key eq 'skip';

    return 'yes' if ($key ~~ Bool && $key == True) || ($key ~~ Str && $key eq 'True');

    return 'no';
}
