#!/bin/bash

TGTOKEN=
KEYSTOREPATH=/tg-bot/app/keystore
BOTMODE=production

if [ ! -z "${RUNMODE}" ] && [ "${RUNMODE}" == "maintenance" ]; then
    BOTMODE=maintenance
fi

# cleanup
for FOLDER in "/git/dcms-raku" "/git/pheix-research" "/git/raku-ethelia"; do
    if [ -d "${FOLDER}" ]; then
        rm -rf ${FOLDER}
    fi;
done

# setup git folder
mkdir -p /git/pheix-research
ln -nsf /git/ $HOME

# clone repositories
git clone https://gitlab.com/pheix-research/raku-dossier-prototype.git /git/pheix-research/raku-dossier-prototype
git clone --recurse-submodules -j8 https://gitlab.com/pheix/dcms-raku.git /git/dcms-raku
git clone https://gitlab.com/pheix/raku-ethelia.git /git/raku-ethelia

# setup tg-bot folders
mkdir -p /var/log/tg-bot/
mkdir -p /tg-bot/app/conf/system
mkdir -p /tg-bot/app/conf/eth
mkdir -p /tg-bot/app/staticmaps/animated
ln -nsf /tg-bot/ $HOME
ln -nsf /git/pheix-research/raku-dossier-prototype/bin/conf/dossier.yaml /tg-bot/app/conf/dossier.yaml
ln -nsf /git/dcms-raku/conf/system/eth/PheixDatabase.abi /tg-bot/app/conf/eth/PheixDatabase.abi
ln -nsf /git/dcms-raku/conf/system/eth/PheixDatabase.bin /tg-bot/app/conf/eth/PheixDatabase.bin
ln -nsf /git/dcms-raku/conf/system/eth/PheixAuth.abi /tg-bot/app/conf/eth/PheixAuth.abi
ln -nsf /git/dcms-raku/conf/system/eth/PheixAuth.bin /tg-bot/app/conf/eth/PheixAuth.bin
ln -nsf /tg-bot/app/conf/eth /tg-bot/app/conf/system/eth
ln -nsf /mnt $KEYSTOREPATH

# populate tg-bot config
echo "# dossierid;chatid;json" > /tg-bot/app/conf/system/dossiers.tnk

echo "Telegram API token, please >>>"

while [ -z "${TGTOKEN}" ]; do
    read -r TGTOKEN

    if [ -z "${TGTOKEN}" ]; then
        echo "blank Telegram API token, try again"
    fi
done

echo "{ \"token\": \"${TGTOKEN}\", \"keystorepath\": \"${KEYSTOREPATH}\" }" > /tg-bot/app/conf/cfg.json

# run tg-bot
cd /root/git/pheix-research/raku-dossier-prototype/utils/ && RUNMODE=${BOTMODE} ./run-daemon.bash root --nosudo
