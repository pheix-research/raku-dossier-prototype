use v6.d;
use Test;

use Dossier::Transport::X;

my $msg = 'dummy exception message';

plan 2;

use-ok 'Dossier::Transport::X', 'Dossier::Transport::X is used ok';

throws-like { Dossier::Transport::X.new(:payload($msg)).throw },
    Exception,
    message => / $msg /,
    'throws exception';

done-testing;
