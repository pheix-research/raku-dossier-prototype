#!/bin/bash

SUDO=sudo
USER=$1
LOG=/var/log/tg-bot
APP=$HOME/tg-bot/app
PIDFILE=/tmp/tg-bot.pid
RUNFROMUSER=kostas
BOTMODE=production
BOT=$HOME/git/pheix-research/raku-dossier-prototype/bin/trivial-bot.raku

if [ ! -z "${USER}" ]; then
    RUNFROMUSER=${USER}
fi

if [ ! -z "${RUNMODE}" ] && [ "${RUNMODE}" == "maintenance" ]; then
    BOTMODE=maintenance
fi

echo "running bot from ${RUNFROMUSER} user in ${BOTMODE} mode";

if [ ! -z "$2" ] && [ "$2" == "--nosudo" ] ; then
    SUDO=
fi


if [ -f "${PIDFILE}" ]; then
    PID=`cat ${PIDFILE}`

    echo "found previously run bot from ${PIDFILE}: $PID";

    if [ ! -z "{$PID}" ]; then
        if [ $PID -gt 0 ]; then
            kill -TERM $PID
        fi
    fi

    rm -f ${PIDFILE}
fi;

if [ ! -d "${LOG}" ]; then
    echo "no ${LOG} found";

    exit 1;
fi;

if [ ! -d "${APP}" ]; then
    echo "no ${APP} found";

    exit 2;
fi;

if [ ! -f "${BOT}" ]; then
    echo "no ${BOT} installed";

    exit 2;
fi;

# run from privileged user
$SUDO daemonize -a -e ${LOG}/error.log -o ${LOG}/bot.log -u ${RUNFROMUSER} -p /tmp/tg-bot.pid -c ${APP} -v ${BOT} --mode=${BOTMODE}
