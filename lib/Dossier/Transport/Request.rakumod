unit class Dossier::Transport::Request;

use JSON::Fast;
use HTTP::UserAgent;
use HTTP::Request;
use HTTP::Request::Common;
use Dossier::Transport::X;

has HTTP::UserAgent $.ua = HTTP::UserAgent.new;
has Str             $.endpoint is rw;
has Bool            $.debug is default(False);

method !prepare_request(:$data!, Str :$method, Str :$content_type) returns HTTP::Request {
    Dossier::Transport::X.new(:payload('no endpoint')).throw unless $!endpoint;

    my $request;

    if $data ~~ Hash {
        $request = HTTP::Request.new(|%($method => $!endpoint));

        $request.header.field(
            Content-Type => $content_type,
            Connection   => 'close',
        );

        $request.add-content(to-json($data));
    }
    elsif $data ~~ Blob && $method eq 'POST' {
        $request = POST(
            $!endpoint,
            Content-Type => $content_type,
            content      => $data,
        );
    }
    else {
        Dossier::Transport::X.new(:payload('unsupported data type in request')).throw;
    }

    return $request;
}

method send(:$data!, Str :$method = 'POST', Str :$content_type = 'application/json') returns Hash {
    my %ret;
    my $response;

    my $request = self!prepare_request(:$data, :$method, :$content_type);

    Dossier::Transport::X.new(:payload('no request')).throw unless $request;

    try {
        $response = $!ua.request($request);

        CATCH {
            default {
                dd($request);

                .throw;
            }
        }
    }

    Dossier::Transport::X.new(:payload('no response')).throw unless $response;

    if $response.code == 200 {
        my Str $body = $response.content // $response.gist;

        sprintf(">>>%s\n===\n<<<%s", $request, $body).say if $!debug;

        try {
            %ret = from-json($body);

            CATCH {
                default {
                    my $e = .message;
                    my $m = sprintf("%s\n---\n%s", $e, $body);

                    Dossier::Transport::X.new(:payload($m)).throw;
                }
            };
        }

        Dossier::Transport::X.new(:payload('blank response')).throw unless %ret.keys.elems;
        Dossier::Transport::X.new(:payload(%ret.gist)).throw if %ret<error_code>;
    }
    else {
        my $code  = $response<error_code> // $response.code;
        my $error = $response<description> ??
            sprintf("%s\n%s", $response.status-line, $response<description>) !!
                $response.status-line;

        Dossier::Transport::X.new(:payload(sprintf("code %s: %s\n%s\n%s", $code, $error, $data ~~ Blob ?? sprintf("%s...", $request.gist.substr(0, 2048)) !! $request.Str, $response.content.gist))).throw;
    }

    return %ret;
}
