#!/usr/bin/perl -w

use feature 'say';

use strict;
use JSON::PP;
use Term::ANSIColor;

use IO::Socket::UNIX qw(SOCK_STREAM);

use constant {
    SOCK_PATH => '/tmp/dossier.ipc',
    CHATID    => int(rand(1_000_000_000)),
};

while(1) {
    my $json;
    my $response;

    my $client = IO::Socket::UNIX->new(Type => SOCK_STREAM, Peer => SOCK_PATH)
        or die sprintf("Can't connect to server: %s\n", $!);

    $client->autoflush(1);

    print($client encode_json({get_dossier_question => 1, chatid => CHATID}))
        or die "cannot send dossier question requests to IPC server\n";

    while (my $line = <$client>) {
        if ($line){
            chomp($line);

            $response .= $line;
        }

        my $json_status = eval { $json = decode_json($response); 1; }
          or do {
              my $e = $@;
          };

        last if $json_status;
    }

    $client->close();

    if ($response && $json) {
        say sprintf("%s [%s]", colored(['bold green'], $json->{dossier}->{question}), colored(['yellow'], join(q{/}, @{$json->{dossier}->{toggles}})));

        exit unless $json->{dossier}->{nodeid} && $json->{dossier}->{nodeid} > 0;

        my $answer = readline(STDIN);

        chomp($answer);

        my $client = IO::Socket::UNIX->new(Type => SOCK_STREAM, Peer => SOCK_PATH)
            or die sprintf("Can't connect to server: %s\n", $!);

        print($client encode_json({dossier_answer => $answer, chatid => CHATID, nodeid => $json->{dossier}->{nodeid}}))
            or die "cannot send dossier answer requests to IPC server\n";

        $client->close();
    }
    else {
        last;
    }
}

1;
