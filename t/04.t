use v6.d;
use Test;

use Dossier::TelegramBot;
use Dossier::Transport::Request;

plan 4;

use-ok 'Dossier::TelegramBot', 'Dossier::TelegramBot';

my $api    = Dossier::Transport::Request.new(:endpoint('http://127.0.0.1:12345'));
my $tg_bot = Dossier::TelegramBot.new(:api($api), :token('deadbeef'));

ok $api, 'api object';
ok $tg_bot, 'bot object';

dies-ok {
    $api.send(:data({login => 'login', password => '1234567890'}), :method('PUT'))
}, 'dies on fake endpoint';

done-testing;
