use v6.d;
use Test;

use Dossier;
use Dossier::Transport::MockSock;

plan 2;

subtest {
    plan 31;

    my $dossier = Dossier.new(:test(True), :cpath('./x/conf.yaml'));

    ok $dossier, 'Dossier object';

    my $gname  = 'dossier';
    my $config = $dossier.load_config;
    my $graph  = $config<chat>{$gname};
    my $target = 1;
    my $chatid = 1;

    ok $dossier.activate_dossier(:$chatid, :graphname($gname)), 'activate dossier';

    while (1) {
        my $accepted = {
            sock      => $dossier.mockedsock,
            chatid    => $chatid,
            graphname => $gname,
            nodeid    => $target
        };

        lives-ok { $dossier.reach(:$accepted, :$graph, :$target) },
            sprintf("reach at iteration %02d (chatid=%d, graphname=%s)", $target, $chatid, $gname);

        if (my $msg = $dossier.reach(:$accepted, :$graph, :$target)) {
            ok ($msg<dossier><question><en>:exists), sprintf("get question <%s>", $msg<dossier><question><en>);

            my $toggle = $dossier.remap_toggle_name(:key($msg<dossier><toggles>[0]));
            $accepted<answer> = $toggle ne 'skip' ?? $toggle !! 'Sample test answer!';

            lives-ok { $dossier.reach(:$accepted, :$graph, :$target ) },
                sprintf("iteration %02d (chatid=%d, graphname=%s) answer %s", $target, $chatid, $gname, ~$accepted<answer>);

            $target++;
        }
        else {
            last;
        }
    }

    ok $dossier.reset_dossier(:$chatid), 'reset dossier';
}, 'check dossier graph';

subtest {
    plan 10;

    my $dossier = Dossier.new(:test(True), :cpath('./x/conf.yaml'));

    ok $dossier, 'Dossier object';

    my $gname  = 'geolocation';
    my $config = $dossier.load_config;
    my $graph  = $config<chat>{$gname};
    my $target = 1;
    my $chatid = 1;

    ok $dossier.activate_dossier(:$chatid, :graphname($gname)), 'activate dossier';

    while (1) {
        my $accepted = {
            sock      => $dossier.mockedsock,
            chatid    => $chatid,
            graphname => $gname,
            nodeid    => $target
        };

        lives-ok { $dossier.reach(:$accepted, :$graph, :$target) },
            sprintf("reach at iteration %02d (chatid=%d, graphname=%s)", $target, $chatid, $gname);

        if (my $msg = $dossier.reach(:$accepted, :$graph, :$target)) {
            ok ($msg<dossier><question><en>:exists), sprintf("get question <%s>", $msg<dossier><question><en>);

            my $toggle = $dossier.remap_toggle_name(:key($msg<dossier><toggles>[0] // 'skip'));
            $accepted<answer> = $toggle ne 'skip' ?? $toggle !! 'Sample test answer!';

            lives-ok { $dossier.reach(:$accepted, :$graph, :$target ) },
                sprintf("iteration %02d (chatid=%d, graphname=%s) answer %s", $target, $chatid, $gname, ~$accepted<answer>);

            $target++;
        }
        else {
            last;
        }
    }

    ok $dossier.reset_dossier(:$chatid), 'reset dossier';
}, 'check geolocation graph';

done-testing;
