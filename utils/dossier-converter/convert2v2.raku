#!/usr/bin/env raku

constant PHEIXPATH = "$*HOME/git/pheix-pool/core-perl6";

use lib sprintf("%s/lib", PHEIXPATH);
use lib "$*HOME/git/pheix-research/raku-dossier-prototype/lib";

use Pheix::Model::Database::Access;
use Pheix::Model::JSON;

use JSON::Fast;
use MIME::Base64;

constant DATABASE_OBJ_V1 = Pheix::Model::Database::Access.new(:table('dossiers.v1'), :fields(<dossierid chatid json>));
constant DATABASE_OBJ_V2 = Pheix::Model::Database::Access.new(:table('dossiers.v2'), :fields(<dossierid chatid json>));

for DATABASE_OBJ_V1.get_all(:fast(True)).values -> $record {
    next unless $record<dossierid> && $record<chatid> && $record<json>;

    my $decoded;

    try {
        $decoded = from-json(MIME::Base64.new.decode-str($record<json>));

        CATCH {
            default {
                my $e = .message;

                sprintf("[FAILURE] restore dossier for chatid %d: %s", $record<chatid>, $e).say;

                next;
            }
        }
    }

    if $decoded {
        my $dbstatus = DATABASE_OBJ_V2.insert({
            dossierid => $record<dossierid>,
            chatid    => $record<chatid>,
            json      => MIME::Base64.new.encode-str(to-json({dossier => $decoded}), :oneline),
        });

        die 'insert to V2 database failure' unless $dbstatus;
    }
}
