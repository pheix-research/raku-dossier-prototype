use v6.d;
use Test;

use Dossier;
use Dossier::Transport::MockSock;

constant ETALON_ONCREATE = {
    payload => {
        date   => ~DateTime.new(now),
        caller => 'ONCREATE',
    }
};

constant ETALON_ONUPDATE = {
    payload => {
        date   => ~DateTime.new(now),
        caller => 'ONUPDATE',
    }
};

plan 2;

subtest {
    plan 4;

    my $dossier = Dossier.new(:test(True), :cpath('./x/conf.yaml'));

    ok $dossier, 'Dossier object';

    my $gname  = 'dossier';
    my $config = $dossier.load_config;
    my $graph  = $config<chat>{$gname};

    lives-ok {
        $dossier.reach(
            :accepted(
                {
                    sock   => $dossier.mockedsock,
                    chatid => 1,
                    answer => 'ONCREATE minimal chars',
                    nodeid => 9
                }
            ),
            :$graph,
            :target(9)
        );
    }, 'oncreate default callback';

    ok $dossier.reset_dossier(:chatid(1)), 'reset dossier';

    lives-ok {
        $dossier.reach(
            :accepted(
                {
                    sock   => $dossier.mockedsock,
                    chatid => 1,
                    answer => 'ONUPDATE minimal chars',
                    nodeid => 9
                }
            ),
            :$graph,
            :target(9)
        );
    }, 'onupdate default callback';
}, 'check default callbacks';

subtest {
    plan 6;

    my $dossier = Dossier.new(
        :test(True),
        :cpath('./x/conf.yaml'),
        :oncreate_callback(&oncreate),
        :onupdate_callback(&onupdate),
    );

    ok $dossier, 'Dossier object';

    my $gname  = 'dossier';
    my $config = $dossier.load_config;
    my $graph  = $config<chat>{$gname};

    lives-ok {
        $dossier.reach(
            :accepted(
                {
                    sock   => $dossier.mockedsock,
                    chatid => 1,
                    answer => 'ONCREATE minimal chars',
                    nodeid => 9
                }
            ),
            :$graph,
            :target(9)
        );
    }, 'oncreate callback';

    is-deeply $dossier.get_dossier(:chatid(1))<data><oncreate>, ETALON_ONCREATE, 'oncreate data';

    ok $dossier.reset_dossier(:chatid(1)), 'reset dossier';

    lives-ok {
        $dossier.reach(
            :accepted(
                {
                    sock => $dossier.mockedsock,
                    chatid => 1,
                    answer => 'ONUPDATE minimal chars',
                    nodeid => 9
                }
            ),
            :$graph,
            :target(9)
        );
    }, 'onupdate callback';

    is-deeply $dossier.get_dossier(:chatid(1))<data><onupdate>, ETALON_ONUPDATE, 'onupdate data';
}, 'check user-defind callbacks';

done-testing;

sub oncreate(Dossier :$dossier!, Cool :$chatid! --> Hash) { return ETALON_ONCREATE; }
sub onupdate(Dossier :$dossier!, Cool :$chatid! --> Hash) { return ETALON_ONUPDATE; }
