use v6.d;
use Test;

constant PATH = sprintf("%s/t/data/log/transactions", $*CWD);
constant MAXFILES = 24;
plan 1;

my $start = now;

ok transaction_log, 'transaction log';

diag(sprintf("runtime: %.3f second(s)", now - $start));

done-testing;

sub transaction_log returns Bool {
    my @logfiles = PATH.IO.dir.grep({$_.basename ~~ /^ 'ethelia-stations.raku-sgn-' /});

    if @logfiles && @logfiles.elems {
        my @transaction_log;

        for @logfiles.sort.reverse[0..(MAXFILES - 1)].values -> $logfile {
            next unless $logfile;
            
            @transaction_log.push(|$logfile.lines.reverse.map({ $_.split(q{|}, :skip-empty).map({ $_.trim }) }));
        }

        @transaction_log.join("\n").say;

        if @transaction_log && @transaction_log.elems {
            my $formatted_text;

            my $maxpos = MAXFILES;

            while ($maxpos > 0 && !$formatted_text) {
                for @transaction_log[0 .. $maxpos].values -> $trx {
                    if $trx && $trx.elems && $trx[4] ~~ / '0x' <xdigit> ** 64 / {
                        my $date = $trx[0];

                        $date ~~ s/\.\d+$//;
                        $date ~~ s/\s/T/;

                        my $link = sprintf("[%s…](https://holesky.etherscan.io/tx/%s)", $trx[4].substr(0, 12), $trx[4]);

                        $formatted_text ~= sprintf("• %s: %s\n", DateTime.new($date, formatter => { sprintf "%04d/%02d/%02d %s", .year, .month, .day, .hh-mm-ss }).in-timezone($*TZ), $link);
                    }
                }

                $formatted_text = Empty unless $formatted_text.chars < 4096;
                $maxpos--;
            }

            if $formatted_text && $formatted_text.chars {
                return True;
            }
        }
    }

    return False;
}
