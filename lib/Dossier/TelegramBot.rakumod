unit class Dossier::TelegramBot;

use Dossier::Transport::Request;
use Dossier::Transport::X;

has Str $.endpoint is default("https://api.telegram.org/bot%s/%s");
has Str $.token;

has Dossier::Transport::Request $.api;

method getMe returns Hash {
    return self.api_request(:method(&?ROUTINE.name));
}

method approveChatJoinRequest(Hash $payload!) returns Hash {
    return self.api_request(:method(&?ROUTINE.name), :data($payload));
}

method getUpdates(Hash $payload!) returns Hash {
    return self.api_request(:method(&?ROUTINE.name), :data($payload));
}

method sendChatAction(Hash $payload!) returns Hash {
    return self.api_request(:method(&?ROUTINE.name), :data($payload));
}

method sendMessage(Hash $payload!) returns Hash {
    return self.api_request(:method(&?ROUTINE.name), :data($payload));
}

method sendPhoto(Blob :$data!, Str :$content_type!) returns Hash {
    return self.api_request(:method(&?ROUTINE.name), :$data, :$content_type);
}

method sendAnimation(Blob :$data!, Str :$content_type!) returns Hash {
    return self.api_request(:method(&?ROUTINE.name), :$data, :$content_type);
}

multi method api_request(Str :$method!, Hash :$data = {}) returns Hash {
    Dossier::Transport::X.new(:payload('invalid method')).throw unless $method ~~ m:i/^<[a .. z]>+$/;

    Dossier::Transport::X.new(:payload('missed token')).throw unless $!token;

    Dossier::Transport::X.new(:payload('no API object')).throw unless $!api;

    $!api.endpoint = sprintf($!endpoint, $!token, $method);

    return $!api.send(:$data);
}

multi method api_request(Str :$method!, Blob :$data!, Str :$content_type!) returns Hash {
    Dossier::Transport::X.new(:payload('invalid method')).throw unless $method ~~ m:i/^<[a .. z]>+$/;

    Dossier::Transport::X.new(:payload('missed token')).throw unless $!token;

    Dossier::Transport::X.new(:payload('no API object')).throw unless $!api;

    $!api.endpoint = sprintf($!endpoint, $!token, $method);

    return $!api.send(:$data, :$content_type);
}
