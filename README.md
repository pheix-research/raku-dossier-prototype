# Dossier

This is `Dossier` module based on trivial YAML graph.

## Quick start with Docker

```bash
bash <(curl -L https://gitlab.com/pheix-research/raku-dossier-prototype/-/raw/main/utils/deploy-in-docker.bash)
```

### Maintenance mode

```bash
RUNMODE=maintenance bash <(curl -L https://gitlab.com/pheix-research/raku-dossier-prototype/-/raw/main/utils/deploy-in-docker.bash)
```

## Ethelia pre-launch Telegram channel

![Pre-launch Telegram channel invite](https://gitlab.com/pheix-io/ethelia/-/raw/main/docs/assets/images/prelaunch-telegram-preview.png "Pre-launch Telegram channel invite")

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
