unit class Dossier::Transport::X is Exception;

has $.payload is default('smth went wrong') is rw;

method message {
    sprintf("%s: %s", self.^name, $!payload);
}
