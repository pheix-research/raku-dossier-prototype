use v6.d;
use Test;

use Dossier::Transport::Request;

my $wired = (%*ENV<WITHREALENDPOINT>.defined && %*ENV<WITHREALENDPOINT> == 1) ?? True !! False;

plan 2;

use-ok 'Dossier::Transport::Request', 'Dossier::Transport::Request is used ok';

subtest {
    plan 4;

    if $wired {
        my $data;
        my $origin = {payload => {items => ['dummy']}};

        my $r = Dossier::Transport::Request.new(:endpoint('https://httpbin.org/post'));

        ok $r, 'request object';

        todo 'possibly lack host';
        lives-ok {
            $data = $r.send(:data($origin))
        }, 'request to httpbin';

        ok $data.keys.elems, 'response has keys';
        is-deeply($data<json>, $origin, 'payload');
    }
    else {
        skip-rest('skip remote endpoint test');
    }

}, 'Check request to bin';

done-testing;
