#!/usr/bin/env raku

use lib "$*HOME/git/pheix/dcms-raku/lib";
use lib "$*HOME/git/pheix-research/raku-dossier-prototype/lib";

use Pheix::Model::Database::Access;

use Dossier::TelegramBot;
use Dossier::Transport::Request;

use JSON::Fast;
use MIME::Base64;
use HTTP::Request::FormData;

constant TIMEOUT    = 30;
constant ITER_DELAY = 2;

constant CONFIG   = from-json(sprintf("%s", sprintf("%s/conf/cfg.json", $*CWD).IO.slurp));
constant MESSAGES = CONFIG<messages>;
constant TOKEN    = CONFIG<token>;

constant DATABASE_OBJ = Pheix::Model::Database::Access.new(:table('dossiers'), :fields(<dossierid chatid json>));
constant SENDTO       = @*ARGS[0] && @*ARGS[0] ~~ m:i/^<[0..9]>+$/ ?? @*ARGS[0] !! 0;

my Str $uploaded_animation_file;
my Str $uploaded_image_file;

sub MAIN(Bool :$dryrun = False, Bool :$byids = False, Str :$imagepath, Bool :$sendaction = False) {
    broadcast(:$dryrun, :$byids, :$imagepath, :$sendaction);
}

sub broadcast(Bool :$dryrun, Bool :$byids, Str :$imagepath, Bool :$sendaction) {
    my $chatbot;
    my @maillist;

    my $offset = 0;

    my $api = Dossier::TelegramBot.new(:api(Dossier::Transport::Request.new), :token(TOKEN));

    X::AdHoc.new(:payload(sprintf("no generic API object"))).throw unless $api;

    try {
        $chatbot = $api.getMe;

        CATCH {
            default {
                my $e = .message;

                bot_log(:message(sprintf("[DIED] can not get chatbot: %s", $e)));

                X::AdHoc.new(:payload(sprintf("can not get chatbot: %s", $e))).throw
            }
        }
    }

    my $dossiers_db = restore_dossiers(:dbobj(DATABASE_OBJ));

    bot_log(:message(sprintf("[RESTORE] imported %s previously stored dossier(s)", $dossiers_db.keys.elems)));

    if SENDTO > 0 {
        @maillist.push(SENDTO);

        bot_log(:message(sprintf("[PREPARE] sending %s to single id: %d", $sendaction ?? 'action' !! 'message', @maillist.head)));
    }
    else {
        if $byids {
            my @ids = CONFIG<ids>.List;

            X::AdHoc.new(:payload("no ids to send to")).throw unless @ids.elems;

            @ids.map({ X::AdHoc.new(:payload("no ids to send to")).throw unless $_ ~~ m:i/^<[0..9]>+$/ });

            @maillist = @ids;

            bot_log(:message(sprintf("[PREPARE] sending %s to preconfigured ids: %s", $sendaction ?? 'action' !! 'message', @maillist.join(q{,}))));
        }
        else {
            @maillist = $dossiers_db.keys;

            bot_log(:message(sprintf("[PREPARE] sending %s to id from dossier: %s", $sendaction ?? 'action' !! 'message', @maillist.join(q{,}))));
        }
    }

    X::AdHoc.new(:payload("no chat identifiers")).throw unless @maillist.elems;

    @maillist.map({
        my $chat_id = $_;

        if !$dryrun && send_message(:$api, :chat(%(id => $chat_id)), :message(MESSAGES<latest>), :$imagepath, :$sendaction) {
            bot_log(:message(sprintf("[SEND] %s is successfully sent to %d", $sendaction ?? 'action' !! 'message', $chat_id)));
        }
    });
}

sub send_message(:$api!, :$chat!, :$message!, :$imagepath, :$sendaction) returns Bool {
    my $update;

    try {
        if $imagepath && $imagepath ne q{} && $imagepath.IO.e && $imagepath.IO.f {
            my $fname       = $imagepath.IO;
            my buf8 $binary = $fname.slurp(:bin);

            my $fd = HTTP::Request::FormData.new;

            $fd.add-part('chat_id', $chat<id>, :content-type('plain/text'));
            $fd.add-part('caption', $message, :content-type('plain/text'));
            $fd.add-part('parse_mode', 'Markdown', :content-type('plain/text'));

            if $fname.extension ~~ /gif|mp4/ {
                if !$uploaded_animation_file {
                    $fd.add-part('animation', $binary, :content-type(sprintf("video/%s", $fname.extension)), :filename($fname.basename));

                    $update = $api.sendAnimation(:content_type($fd.content-type), :data($fd.content));

                    if $update && $update<result> && $update<result><animation> && $update<result><animation><file_id> {
                        $uploaded_animation_file = $update<result><animation><file_id>;

                        bot_log(:message(sprintf("[MEMSAVE] animation file %s", $uploaded_animation_file)));
                    }
                }
                else {
                    $fd.add-part('animation', $uploaded_animation_file, :content-type('plain/text'));
                    $api.sendAnimation(:content_type($fd.content-type), :data($fd.content));
                }
            }
            else {
                if !$uploaded_image_file {
                    $fd.add-part('photo', $binary, :content-type(sprintf("image/%s", $fname.extension)), :filename($fname.basename));

                    $update = $api.sendPhoto(:content_type($fd.content-type), :data($fd.content));

                    if $update && $update<result> && $update<result><photo> && $update<result><photo>.head {
                        $uploaded_image_file = ($update<result><photo>.head)<file_id>;

                        bot_log(:message(sprintf("[MEMSAVE] image file %s", $uploaded_image_file)));
                    }
                }
                else {
                    $fd.add-part('photo', $uploaded_image_file, :content-type('plain/text'));
                    $api.sendPhoto(:content_type($fd.content-type), :data($fd.content));
                }
            }
        }
        else {
            if $sendaction {
                $api.sendChatAction(
                    {
                        chat_id    => $chat<id>,
                        action     => 'typing',
                    }
                );
            }
            else {
                $api.sendMessage(
                    {
                        parse_mode => 'Markdown',
                        chat_id    => $chat<id>,
                        text       => $message,
                    }
                );
            }
        }

        CATCH {
            default {
                my $e = .message;
                my $b = .backtrace;

                bot_log(:message(sprintf("[FAILURE] message is not sent to %d: %s", $chat<id>, $e)));

                return False;
            }
        }
    };

    return True;
}

sub restore_dossiers(:$dbobj!) returns Hash {
    my $restored_dossiers = {};

    for $dbobj.get_all(:fast(True)).values -> $record {
        next unless $record<chatid> && $record<json>;

        try {
            $restored_dossiers{$record<chatid>} = from-json(MIME::Base64.new.decode-str($record<json>));

            CATCH {
                default {
                    my $e = .message;

                    bot_log(:message(sprintf("[FAILURE] restore dossier for chatid %d: %s", $record<chatid>, $e)));

                    next;
                }
            }
        }
    }

    return $restored_dossiers;
}

sub bot_log(Str :$message!) returns Bool {

    sprintf("%s: %s",
        DateTime.now(formatter => { sprintf("%04d-%02d-%02d %02d:%02d:%02u.%04u", .year, .month, .day, .hour, .minute, .second.Int, (.second % (.second.Int || .second)) * 10_000) }),
        $message).say;

    return True;
}
