#!/usr/bin/env raku

# Telegram markup:
# \n - newline
# *text* - bold
# _text_ - intalic
# `text` - code
# [text](link) - hyperlink

constant PHEIXPATH = "$*HOME/git/dcms-raku";
constant ADMINCONF = sprintf("%s/conf/addons/custom_path/EmbeddedAdmin/", PHEIXPATH);

use lib sprintf("%s/lib", PHEIXPATH);
use lib "$*HOME/git/pheix-research/raku-dossier-prototype/lib";
use lib "$*HOME/git/raku-ethelia/lib";

use Pheix::Addons::Embedded::Admin;
use Pheix::Model::Database::Access;
use Pheix::Model::JSON;

use Dossier;
use Dossier::TelegramBot;
use Dossier::Transport::Request;
use Node::Ethereum::KeyStore::V3;
use Ethelia::Explorer;
use Node::Ethereum::Keccak256::Native;
use Net::Ethereum;

use JSON::Fast;
use MIME::Base64;
use HTTP::Request::FormData;
use Compress::Bzip2;
use Compress::Bzip2::Raw;
use NativeCall;

constant CONFIG       = from-json(sprintf("%s", sprintf("%s/conf/cfg.json", $*CWD).IO.slurp));
constant KEYSTOREPATH = CONFIG<keystorepath>;
constant TOKEN        = CONFIG<token>;

constant GRAPHNAME    = 'dossier';
constant GEOMARKER    = '📍';
constant QRCODESELFIE = '🤳🏻';
constant WINKING_FACE = '😉';
constant CUPEMOJII    = '🏆';
constant ANONYMOUS    = 'Anonymous';
constant TIMEOUT      = 5;
constant ITER_DELAY   = 2;
constant DATABASE_OBJ = Pheix::Model::Database::Access.new(:table('dossiers'), :fields(<dossierid chatid json>));
constant STARTAMOUNT  = 1500000000000000000;
constant KECCAK256    = Node::Ethereum::Keccak256::Native.new;
constant NETETHEREUM  = Net::Ethereum.new;
constant EVENTLISTTOKEN = '0xd78776d2eec8eb2835e86d7c8bb070abf001543171ffd31f676e6997e6f54f33';
constant DEFAULTLANG    = 'en';
constant EVENTLIFETIME  = CONFIG<eventlifetime> // 3 * 9000;
constant MAXFILES       = 24;

enum Mode <production maintenance>;

sub MAIN (Mode:D :$mode = (production)) {
    run_bot(:$mode);
}

sub run_bot(Mode:D :$mode = (production)) {
    my $chatbot;
    my $offset = CONFIG<offset>.defined ?? CONFIG<offset> !! 0;

    bot_log(:message(sprintf("[OFFSET] found previously processed update with offset %d", CONFIG<offset>)))
        if CONFIG<offset>.defined && CONFIG<offset> > 0;

    $*ERR.out-buffer = False;
    $*OUT.out-buffer = False;

    X::AdHoc.new(:payload(sprintf("invalid keystore path <%s>", KEYSTOREPATH // q{}))).throw unless KEYSTOREPATH && KEYSTOREPATH.IO.d;

    my $api = Dossier::TelegramBot.new(:api(Dossier::Transport::Request.new), :token(TOKEN));

    X::AdHoc.new(:payload(sprintf("no generic API object"))).throw unless $api;

    try {
        $chatbot = $api.getMe;

        CATCH {
            default {
                my $e = .message;

                bot_log(:message(sprintf("[DIED] can not get chatbot: %s", $e)));

                X::AdHoc.new(:payload(sprintf("can not get chatbot: %s", $e))).throw
            }
        }
    }

    my $dossiers_db = restore_dossiers(:dbobj(DATABASE_OBJ));

    my $cpath   = sprintf("%s/conf/dossier.yaml", $*CWD);
    my $dossier = Dossier.new(
        :cpath($cpath),
        :dossiers($dossiers_db),
        :dbobj(DATABASE_OBJ),
        :oncreate_callback(&dossier_oncreate_callback)
    );

    bot_log(:message(sprintf("[RESTORE] imported %s previously stored dossier(s)", $dossiers_db.keys.elems)));

    X::AdHoc.new(:payload(sprintf("config path <%s> is not found", $cpath // q{}))).throw
        unless $dossier.cpath && $dossier.cpath.IO.e && $dossier.cpath.IO.f;

    my $config = $dossier.load_config;

    X::AdHoc.new(:payload('no chat found')).throw unless $config<chat> && $config<chat>.elems;

    my $graph = $config<chat><dossier>;

    X::AdHoc.new(:payload('no graph found')).throw unless $graph && $graph.elems;

    my $g_head = $graph.head;
    my $g_tail = $graph.tail;

    X::AdHoc.new(:payload('no start node')).throw unless $g_head<type> && $g_head<type> eq 'start';
    X::AdHoc.new(:payload('no end node')).throw   unless $g_tail<type> && $g_tail<type> eq 'finish';

    my $bot_loop = supply {
        whenever Supply.interval(ITER_DELAY, 0) {
            my $updates;

            try {
                my $args = { timeout => TIMEOUT, ($offset > 0 ?? %(offset => $offset) !! %()) };

                $updates = $api.getUpdates($args);

                CATCH {
                    default {
                        my $e = .message;

                        bot_log(:message(sprintf("[FAILURE] bot can not get updates: %s", $e)));
                    }
                }
            }

            if $updates && $updates<ok> {
                for $updates<result>.values -> $update {
                    $offset = $update<update_id> + 1 if $update<update_id> >= $offset;

                    my $thread = Thread.start(
                        :name(sprintf("TG bot handler for %d update", $offset)),
                        sub { process(:$api, :$dossier, :$update, :$mode) }
                    );

                    bot_log(:message(sprintf("thread 0x%08x: new update %d for bot \{%s\}", $thread.id, $offset, $update.keys.sort.join(q{,}))));
                }
            }
        }
    };

    bot_log(:message(sprintf("TG bot is started in %s mode with %d nodes in dossier", $mode, $graph.elems)));

    react {
        whenever signal(SIGTERM,SIGINT,SIGQUIT,SIGHUP,SIGSEGV) -> $sig {
            bot_log(:message(sprintf("TG bot is forced to finish via %s signal", $sig)));

            if $sig eq 'SIGSEGV' {
                my $conf      = CONFIG;
                $conf<offset> = $offset;

                sprintf("%s/conf/cfg.json", $*CWD).IO.spurt(to-json($conf));
            }

            exit;
        };
        whenever $bot_loop {};
    }
}

sub process(:$api!, :$dossier!, :$update!, Mode:D :$mode = (production)) returns Bool {
    my $text;
    my $chat;
    my $user;
    my $lang;

    if $update<chat_join_request> {
        return False unless $update<chat_join_request><chat> && $update<chat_join_request><from>;

        my $channel         = $update<chat_join_request><chat>;
        my $private_chat_id = $update<chat_join_request><user_chat_id>;
        my $from            = $update<chat_join_request><from>;
        my $lang            = $from<language_code> // DEFAULTLANG;

        try {
            $api.approveChatJoinRequest({chat_id => $channel<id>, user_id => $from<id> });

            $api.sendMessage({chat_id => $private_chat_id, text => sprintf(check_vocabulary(:$dossier, :$lang, :text('welcometext')), $from<first_name>)});

            bot_log(:message(sprintf("User %s is accepted on %s channel", $from<first_name>, $channel<title>)));

            CATCH {
                default {
                    my $e = .message;
                    my $b = .backtrace;

                    bot_log(:message(sprintf("[FAILURE] can not accept user %s to join channel %s: %s\n%s", $from<first_name>, $channel<title>, $e, $b)));
                }
            }
        }

        return True;
    }

    if $update<callback_query> && $update<callback_query><data> && $update<callback_query><message> {
        $text = $update<callback_query><data>;
        $chat = $update<callback_query><from>;
        $user = $update<callback_query><from>;
        $lang = $user<language_code> // DEFAULTLANG;
    }
    elsif $update<message><location> && $update<message><location>.keys {
        $text = '/geolocation';
        $chat = $update<message><chat>;
        $user = $update<message><from>;
        $lang = $user<language_code> // DEFAULTLANG;
    }
    elsif $update<message><web_app_data> && $update<message><web_app_data>.keys && $update<message><web_app_data><data> {
        $text = sprintf("%d", $update<message><web_app_data><data>.UInt);
        $chat = $update<message><chat>;
        $user = $update<message><from>;
        $lang = $user<language_code> // DEFAULTLANG;
    }
    else {
        $text = $update<message><text>;
        $chat = $update<message><chat>;
        $user = $update<message><from>;
        $lang = $user<language_code> // DEFAULTLANG;
    }

    return False unless $text && $chat && $user;

    bot_log(:message(sprintf("request from chat %d, user %s (from=%s): %s", $chat<id>, ($chat<username> // $chat<first_name> // ANONYMOUS), $user<id>, $text)));

    if $mode eq 'maintenance' {
        $api.sendMessage(
            {
                parse_mode => 'Markdown',
                chat_id    => $chat<id>,
                text       => check_vocabulary(:$dossier, :$lang, :text('botundermaintenance'))
            }
        );

        return True;
    }

    if $text ~~ m:i/^ '/' (<[a..z]>+) \s* (.*) $/ {
        my $chatdossier = $dossier.get_dossier(:chatid($chat<id>), :graphname(GRAPHNAME));
        my $command     = ~$0;
        my $input       = ~$1;

        try {
            my $status = 0;

            if $command eq 'start' && $dossier.activate_dossier(:chatid($chat<id>)) {
                $status = command_start(:$api, :$dossier, :$chat, :$lang);
            }
            elsif $command eq 'reset' && $dossier.activate_dossier(:chatid($chat<id>)) && $dossier.reset_dossier(:chatid($chat<id>)) {
                $status = command_start(:$api, :$dossier, :$chat, :$lang);
            }
            elsif $command eq 'fetch' && command_fetch(:$api, :$dossier, :$chat, :$lang) {
                ;
            }
            elsif $command eq 'map' && is_registration_completed(:$chatdossier) {
                my @screenshots = dir(sprintf("%s/staticmaps", $*CWD)).grep({$_.f && $_.extension eq 'png'}).sort;

                if @screenshots && @screenshots.elems {
                    my $fname       = @screenshots.tail;
                    my buf8 $binary = $fname.slurp(:bin);

                    my $fd = HTTP::Request::FormData.new;

                    $fd.add-part('chat_id', $chat<id>, :content-type('plain/text'));
                    $fd.add-part('photo', $binary, :content-type('image/png'), :filename($fname.basename));
                    $fd.add-part('caption', DateTime.new($fname.modified, :formatter({ sprintf "%04d/%02d/%02d %s", .year, .month, .day, .hh-mm-ss })).in-timezone($*TZ), :content-type('plain/text'));

                    $api.sendPhoto(:content_type($fd.content-type), :data($fd.content));
                }
                else {
                    $api.sendMessage(
                        {
                            chat_id => $chat<id>,
                            text    => check_vocabulary(:$dossier, :$lang, :text('nomap'))
                        }
                    );
                }
            }
            elsif $command eq 'livemap' && ($input ~~ m:i/^ (\d ** 8) $/ || $input eq q{}) && is_registration_completed(:$chatdossier) {
                my $fname;
                my $fpath = sprintf("%s/staticmaps/animated", $*CWD);

                if $input ne q{} {
                    $fname = sprintf("%s/%s-animatedmap.mp4", $fpath, $input).IO;
                }
                else {
                    $fname = $fpath.IO.dir.grep({ $_.basename ~~ / '-animatedmap.mp4' $/ }).sort(*.modified).tail;
                }

                if $fname && $fname ~~ IO && $fname.f {
                    my buf8 $binary = $fname.slurp(:bin);
                    my Str  $date   = DateTime.new($fname.modified, formatter => { sprintf("%04d/%02d/%02d", .year, .month, .day) }).in-timezone($*TZ).Str;

                    my $fd = HTTP::Request::FormData.new;

                    $fd.add-part('chat_id', $chat<id>, :content-type('plain/text'));
                    $fd.add-part('animation', $binary, :content-type('video/mp4'), :filename($fname.basename));
                    $fd.add-part('caption', sprintf(check_vocabulary(:$dossier, :$lang, :text('livemapcaption')), $date), :content-type('plain/text'));

                    $api.sendAnimation(:content_type($fd.content-type), :data($fd.content));
                }
                else {
                    $api.sendMessage(
                        {
                            chat_id => $chat<id>,
                            text    => check_vocabulary(:$dossier, :$lang, :text('nolivemap'))
                        }
                    );
                }
            }
            elsif $command eq 'decode' && ($input ~~ m:i/^ 0x<xdigit>+ $/ || $input eq q{}) && is_registration_completed(:$chatdossier) {
                bot_log(
                    :message(
                        sprintf("decoding transaction input status: %s", ~decode_transaction_input(:$api, :chatid($chat<id>), :$input, :$dossier, :$lang))
                        ));
            }
            elsif $command eq 'decompress' && ($input ~~ m:i/^ 0x<xdigit>+ $/ || $input eq q{}) && is_registration_completed(:$chatdossier) {
                bot_log(
                    :message(
                        sprintf("decompressing Bzip2 row data: %s", ~decompress_row_data(:$api, :chatid($chat<id>), :$input, :$dossier, :$lang))
                        ));
            }
            elsif $command eq 'pm' && is_registration_completed(:$chatdossier) {
                $dossier.activate_dossier(:chatid($chat<id>), :graphname('geolocation'));
                $dossier.reset_dossier(:chatid($chat<id>));

                $status = command_start(:$api, :$dossier, :$chat, :$lang, :graphname('geolocation'));
            }
            elsif $command eq 'providers' && is_registration_completed(:$chatdossier) {
                bot_log(
                    :message(
                        sprintf("fetching data providers: %s", ~providers(:$api, :chatid($chat<id>), :$dossier, :$lang))
                        ));
            }
            elsif $command eq 'trxlog' && is_registration_completed(:$chatdossier) {
                bot_log(
                    :message(
                        sprintf("fetching log with transactions: %s", ~transaction_log(:$api, :chatid($chat<id>), :$dossier, :$lang))
                        ));
            }
            elsif $command eq 'time' && is_registration_completed(:$chatdossier) {
                bot_log(
                    :message(
                        sprintf("fetch current time from server: %s", ~(bot_utils(:$api, :chatid($chat<id>), :command('time'))<status>))
                        ));
            }
            elsif $command eq 'synclog' && is_registration_completed(:$chatdossier) {
                bot_log(
                    :message(
                        sprintf("fetching update system log: %s", ~blockchainsync_log(:$api, :chatid($chat<id>), :$dossier, :$lang))
                        ));
            }
            elsif $command eq 'balance' && is_registration_completed(:$chatdossier) {
                bot_log(
                    :message(
                        sprintf("fetching actual balance on target blockchain account: %s", ~get_balance(:$api, :chatid($chat<id>), :$dossier, :$lang))
                        ));
            }
            elsif $command eq 'help' {
                bot_log(
                    :message(
                        sprintf("show help message: %s", ~show_help(:$api, :chatid($chat<id>), :$dossier, :$lang))
                        ));
            }
            elsif $command eq 'gethlog' && is_registration_completed(:$chatdossier) {
                bot_log(
                    :message(
                        sprintf("fetching geth log: %s", ~geth_log(:$api, :chatid($chat<id>), :$dossier, :$lang))
                        ));
            }
            else {
                $status = process_answer(:$api, :$dossier, :$chat, :text(~$command), :$lang, :$update);
            }

            bot_log(:message(
                sprintf(
                    "successfully processed command %s from %s with status %s",
                    $command, ($chat<username> // $chat<first_name> // ANONYMOUS), $status
                )
            ));

            CATCH {
                default {
                    my $e = .message;
                    my $b = .backtrace;

                    bot_log(:message(sprintf("[FAILURE] can not process command %s for user %s: %s\n%s", $command, ($chat<username> // $chat<first_name> // ANONYMOUS), $e, $b)));

                    return False;
                }
            }
        }
    }
    else {
        try {
            my $status = process_answer(:$api, :$dossier, :$chat, :$text, :$lang);

            bot_log(:message(
                sprintf(
                    "successfully processed answer '%s' from %s with status %s",
                    $text, ($chat<username> // $chat<first_name> // ANONYMOUS), $status
                )
            ));

            CATCH {
                default {
                    my $e = .message;
                    my $b = .backtrace;

                    bot_log(:message(sprintf("[FAILURE] can not process answer '%s' for user %s: %s\n%s", $text, ($chat<username> // $chat<first_name> // ANONYMOUS), $e, $b)));

                    return False;
                }
            }
        }
    }

    return True;
}

sub dossier_question(:$dossier!, :$chat!, Str :$graphname = GRAPHNAME) returns Hash {
    return {
        sock      => $dossier.mockedsock,
        chatid    => $chat<id>,
        graphname => $graphname,
    };
}

sub dossier_answer(:$dossier!, :$chat!, :$answer!, :$target!, Str :$graphname = GRAPHNAME) returns Hash {
    return $target > 0 ?? {
        sock      => $dossier.mockedsock,
        chatid    => $chat<id>,
        graphname => $graphname,
        answer    => $answer,
        nodeid    => $target,
    } !! {};
}

sub process_answer(:$api!, :$dossier!, :$chat!, :$text!, Str :$lang, Hash :$update = {}) returns Bool {
    my $msg;

    my $graphname   = $dossier.get_active_graphname(:chatid($chat<id>));
    my $graph       = $dossier.load_graph(:chatid($chat<id>));
    my $chatdossier = $dossier.get_dossier(:chatid($chat<id>));
    my $accepted    = dossier_answer(:$dossier, :$chat, :answer($text), :target($chatdossier<next> // 1), :$graphname);

    if $accepted {
        $dossier.reach(:accepted($accepted), :$graph, :target($chatdossier<next> // 1));

        $chatdossier = $dossier.get_dossier(:chatid($chat<id>));
        $accepted    = dossier_question(:$dossier, :$chat, :$graphname);

        $msg = $dossier.reach(:accepted($accepted), :$graph, :target($chatdossier<next> // 1));
    }

    my $config   = $dossier.load_config;
    my $language = $lang // $config<default_language> // DEFAULTLANG;

    my $inline_keyboard = get_inline_keyboard(:$dossier, :toggles($msg<dossier><toggles>), :buttons($msg<dossier><buttons>), :lang($language));
    my $keyboard        = get_keyboard(:$dossier, :buttons($msg<dossier><buttons>), :lang($language));

    my $question = ($msg && $msg<dossier> && $msg<dossier><question>{$language}) ??
        $msg<dossier><question>{$language} !!
            Empty;

    $api.sendMessage(
        {
            parse_mode => 'Markdown',
            chat_id => $chat<id>,
            text => $question ??
                sprintf("%s%s", ($msg<dossier><answer> && $graphname eq GRAPHNAME) ?? (sprintf("_%s_\n\n", check_vocabulary(:$dossier, :$lang, :text($msg<dossier><answer>)))) !! q{}, $question) !!
                    registration_details(:$dossier, :chatid($chat<id>), :lang($language), :$update),
            ($question && $msg<dossier><toggles> && $msg<dossier><toggles>.elems) ??
                %(reply_markup => {inline_keyboard => $inline_keyboard}) !!
                    ($question && $msg<dossier><buttons> && $msg<dossier><buttons>.elems ??
                        %(reply_markup => {one_time_keyboard => True, keyboard => $keyboard}) !!
                            %(reply_markup => {remove_keyboard => True}))
        }
    );

    return True;
}

sub command_fetch(:$api!, :$dossier!, :$chat!, Str :$lang, Str :$graphname = GRAPHNAME) returns Bool {
    my $chatdossier = $dossier.get_dossier(:chatid($chat<id>), :$graphname);

    return False unless $chatdossier<next>.defined && $chatdossier<next> == 0;

    return False unless $chatdossier<nodes> && $chatdossier<nodes>.keys;

    my $answers   = $chatdossier<nodes>.keys.sort({.Int});
    my $anslength = $answers.elems;
    my $config    = $dossier.load_config;
    my $language  = $lang // $config<default_language> // DEFAULTLANG;

    my $text;

    for $answers.kv -> $index, $key {
        my $dossier_rec = $chatdossier<nodes>{$key};
        my $question    = $dossier_rec<question> ~~ Str ?? $dossier_rec<question> !! $dossier_rec<question>{$language};

        next unless $dossier_rec && $question && $dossier_rec<answer>;

        $text ~= sprintf("— *%s*\n_%s_%s", $question, check_vocabulary(:$dossier, :$lang, :text($dossier_rec<answer>)), $index < ($anslength - 1) ?? "\n\n" !! q{});
    }

    return False unless $text;

    $api.sendMessage(
        {
            parse_mode   => 'Markdown',
            chat_id      => $chat<id>,
            text         => $text,
            reply_markup => {remove_keyboard => True}
        }
    );

    return True;
}

sub command_start(:$api!, :$dossier!, :$chat!, Str :$lang, Str :$graphname = GRAPHNAME) returns Bool {
    my $msg;

    $dossier.activate_dossier(:chatid($chat<id>), :$graphname);

    my $accepted    = dossier_question(:$dossier, :$chat, :$graphname);
    my $chatdossier = $dossier.get_dossier(:chatid($chat<id>));
    my $config      = $dossier.load_config;
    my $language    = $lang // $config<default_language> // DEFAULTLANG;

    $msg = $dossier.reach(:accepted($accepted), :graph($dossier.load_graph(:chatid($chat<id>))), :target(1))
        unless $chatdossier<next>.defined && $graphname eq GRAPHNAME;

    my $inline_keyboard  = get_inline_keyboard(:$dossier, :toggles($msg<dossier><toggles>), :buttons($msg<dossier><buttons>), :lang($language));
    my $keyboard         = get_keyboard(:$dossier, :buttons($msg<dossier><buttons>), :lang($language));
    my $fallback_message = ($chatdossier<next>.defined && $chatdossier<next> == 0) ??
        sprintf(check_vocabulary(:$dossier, :$lang, :text('fallbackok')), ($chat<username> // $chat<first_name> // ANONYMOUS)) !!
            sprintf(check_vocabulary(:$dossier, :$lang, :text('fallbackfail')), ($chat<username> // $chat<first_name> // ANONYMOUS));

    $api.sendMessage(
        {
            parse_mode => 'Markdown',
            chat_id => $chat<id>,
            text => ($msg && $msg<dossier> && $msg<dossier><question>{$language}) ??
                sprintf("%s%s", ($msg<dossier><answer> && $graphname eq GRAPHNAME) ?? (sprintf("_%s_\n\n", $msg<dossier><answer>)) !! q{}, $msg<dossier><question>{$language}) !!
                    $fallback_message,
            ($msg && $msg<dossier> && $msg<dossier><question>{$language} && $msg<dossier><toggles> && $msg<dossier><toggles>.elems) ??
                %(reply_markup => {inline_keyboard => $inline_keyboard}) !!
                    ($msg && $msg<dossier> && $msg<dossier><question>{$language} && $msg<dossier><buttons> && $msg<dossier><buttons>.elems) ??
                        %(reply_markup => {one_time_keyboard => True, keyboard => $keyboard}) !!
                            %(reply_markup => {remove_keyboard => True})
        }
    );

    return True;
}

sub restore_dossiers(:$dbobj!) returns Hash {
    my $restored_dossiers = {};

    for $dbobj.get_all(:fast(True)).values -> $record {
        next unless $record<chatid> && $record<json>;

        try {
            $restored_dossiers{$record<chatid>} = from-json(MIME::Base64.new.decode-str($record<json>));

            CATCH {
                default {
                    my $e = .message;
                    my $b = .backtrace;

                    bot_log(:message(sprintf("[FAILURE] restore dossier for chatid %d: %s\n%s", $record<chatid>, $e, $b)));

                    next;
                }
            }
        }
    }

    return $restored_dossiers;
}

sub bot_log(Str :$message!) returns Bool {
    sprintf("%s: %s",
        DateTime.now(formatter => { sprintf("%04d-%02d-%02d %02d:%02d:%02u.%04u", .year, .month, .day, .hour, .minute, .second.Int, (.second % (.second.Int || .second)) * 10_000) }),
        $message).say;

    return True;
}

sub get_inline_keyboard(Dossier :$dossier!, :$toggles, :$buttons, Str :$lang) returns List {
    return unless $toggles.elems || $buttons.elems;

    my $ik;

    for $toggles.values -> $toggle {
        my $toggle_text = check_vocabulary(:$dossier, :$lang, :text($toggle));

        $ik.push({
            text          => $toggle_text,
            callback_data => sprintf("/%s", $toggle),
        })
    }

    return [ $ik, [] ];
}

sub get_keyboard(Dossier :$dossier!, :$buttons, Str :$lang) returns List {
    return unless $buttons.elems;

    my $keyboard;

    for $buttons.values -> $button {
        if $button ~~ Hash {
            my $webapp = $button.keys.head;

            $keyboard.push({
                text    => sprintf(check_vocabulary(:$dossier, :$lang, :text($webapp)), QRCODESELFIE),
                web_app => {
                    url => $button{$webapp}
                }
            })
        }
        else {
            $keyboard.push({
                text => sprintf(check_vocabulary(:$dossier, :$lang, :text($button)), GEOMARKER),
                $button => True
            });
        }
    }

    return [ $keyboard, [] ];
}

sub dossier_oncreate_callback(Dossier :$dossier!, Cool :$chatid!) returns Hash {
    return {} unless $dossier.get_active_graphname(:$chatid) eq GRAPHNAME;

    my $ksobj = Node::Ethereum::KeyStore::V3.new;
    my $creds = $ksobj.credentials;

    $creds<password> ~~ s:g/\`/@/;

    my $keystore = $ksobj.keystore(:password($creds<password>), :secret($creds<privatekey>));

    (my $dateprefix  = ~DateTime.new(now, timezone => $*TZ)) ~~ s:g/':'/-/;
    my $keystorepath = sprintf("%s/UTC--%s--%s.json", KEYSTOREPATH, $dateprefix, $keystore<address>);

    $ksobj.save(:$keystore, :path($keystorepath));

    X::AdHoc.new(:payload(sprintf("can not save keystore file %s for chatid %d", $keystorepath, $chatid))).throw unless $keystorepath.IO.f;

    my $agw = Pheix::Model::Database::Access.new(
        :table('auth-node'),
        :fields([]),
        :txwaitsec(1),
        :jsonobj(init_pheix_config(:path(ADMINCONF)))
    );

    X::AdHoc.new(:payload('wrong auth gateway')).throw unless $agw && $agw.dbswitch == 1;

    my $initial_balance_trx_data = {
        from  => $agw.chainobj.ethacc,
        to    => $keystore<address>,
        value => '0x' ~ STARTAMOUNT.base(16),
    };

    bot_log(:message(sprintf("log to auth gateway via %s", $agw.chainobj.ethacc)));

    X::AdHoc.new(:payload(sprintf("can not unlock account %s", $agw.chainobj.ethacc))).throw unless $agw.chainobj.unlock_account;

    bot_log(:message(sprintf("transfer balance: %s", $initial_balance_trx_data.gist)));

    my $tx = $agw.chainobj.ethobj.eth_sendTransaction($initial_balance_trx_data);

    X::AdHoc
        .new(:payload(sprintf("can not transfer initial balance from %s to %s", $agw.chainobj.ethacc, $keystore<address>)))
        .throw unless $tx && $tx ~~ m:i/^ 0x<xdigit>**64 $/;

    return {
        address    => $keystore<address>,
        password   => $creds<password>,
        privatekey => $creds<privatekey>,
        balancetrx => $tx,
    };
}

sub is_registration_completed(:$chatdossier!) returns Bool {
    return False unless $chatdossier && $chatdossier<data> && $chatdossier<data><oncreate>;

    return False unless $chatdossier<data><oncreate> ~~ Hash && $chatdossier<data><oncreate>.keys.elems == 4;

    my $addr = $chatdossier<data><oncreate><address>  // q{};
    my $pass = $chatdossier<data><oncreate><password> // q{};

    return False unless $addr ~~ m:i/^ 0x<xdigit>**40 $/ && $pass ~~ m/.+/;

    return True;
}

sub registration_details(:$dossier!, Cool :$chatid!, Str :$lang, Hash :$update = {}) returns Str {
    my $graphname   = $dossier.get_active_graphname(:$chatid);
    my $chatdossier = $dossier.get_dossier(:$chatid, :$graphname);

    return geolocation_completed(:$dossier, :$lang, :$chatid, :$update) if $graphname eq 'geolocation';

    return check_vocabulary(:$dossier, :$lang, :text('dossierdone')) unless is_registration_completed(:$chatdossier);

    return sprintf(check_vocabulary(:$dossier, :$lang, :text('gethtext')), $chatdossier<data><oncreate><address>, $chatdossier<data><oncreate><password>);
}

sub init_pheix_config(Str :$path) returns Pheix::Model::JSON {
    X::AdHoc.new(:payload(sprintf("Pheix config path %s is not found", $path))).throw unless $path.IO.d;

    return Pheix::Model::JSON.new(:path($path)).set_entire_config(:addon('Pheix'));
}

sub decode_transaction_input(Dossier :$dossier!, :$api!, Cool :$chatid, Str :$input!, Str :$lang) returns Bool {
    my $error;
    my $decoded  = {};

    if $input ~~ m:i/^ 0x<xdigit>+ $/ {
        my $explorer = Ethelia::Explorer.new(:abi(sprintf("%s/conf/eth/PheixDatabase.abi", $*CWD)));

        try {
            $decoded = $explorer.decode(:function('set'), :inputdata($input));

            CATCH {
                default {
                    $error = .message;

                    try {
                        $decoded = $explorer.decode(:function('insert'), :inputdata($input));

                        CATCH {
                            default {
                                $error ~= sprintf("\n%s", .message);
                            }
                        }
                    }
                }
            }
        }
    }
    else {
        $error = check_vocabulary(:$dossier, :$lang, :text('decodeusage'));
    }

    if $decoded && $decoded.keys && ($decoded<rowdata>:exists) {
        <reference length type>.map({ $decoded<rowdata>{$_}:delete });

        (my $json = to-json($decoded<rowdata>)) ~~ s:i/\`//;

        $api.sendMessage(
            {
                parse_mode => 'Markdown',
                chat_id    => $chatid,
                text       => sprintf("```javascript\n%s```", $json);
            }
        );

        return True;
    }

    $api.sendMessage({
        parse_mode => 'Markdown',
        chat_id    => $chatid,
        text       => sprintf(check_vocabulary(:$dossier, :$lang, :text('decodefailure')), $error);
    });

    return False;
}

sub store_pm25_sensorvalue(:$dossier!, Hash :$update!, UInt :$sensorvalue!) returns Bool {
    return False unless ($update<message><location><longitude>:exists) &&
        ($update<message><location><latitude>:exists) &&
            $update<message><location><longitude>.floor.Int == 74 &&
                $update<message><location><latitude>.floor.Int == 42;

    $dossier.activate_dossier(:chatid($update<message><chat><id>), :graphname(GRAPHNAME));

    my $transport   = Dossier::Transport::Request.new(:endpoint('https://pheix.org/api'));
    my $chatdossier = $dossier.get_dossier(:chatid($update<message><chat><id>));
    my $authnode    = Pheix::Addons::Embedded::Admin.new(:jsonobj(init_pheix_config(:path(ADMINCONF)))).get_authnode;

    return False unless $authnode;

    my %validate = $authnode.auth_on_blockchain(
        :addr($chatdossier<data><oncreate><address> // q{}),
        :pwd($chatdossier<data><oncreate><password> // q{})
    );

    if (%validate<status>:exists) && %validate<status> {
        bot_log(:message(sprintf("access granted for token %s",  ~%validate<tx>)));
    }
    else {
        bot_log(:message(sprintf("[FAILURE] auth on blockchain for %s is failed: %s", $chatdossier<data><oncreate><address>, %validate.gist)));

        return False;
    }

    my $data = {
        credentials => {
            token => ~%validate<tx>
        },
        method  => "GET",
        route   => "/api/ethelia/event/store",
        payload => {
            code   => NETETHEREUM.buf2hex(KECCAK256.keccak256(:msg(~$update<message><chat><id>)).subbuf(0, 16)).lc,
            topic  => "TGBotReport",
            payload => {
                device       => ~$*KERNEL,
                application  => ~$*PROGRAM-NAME,
                sensorvalue  => ~$sensorvalue,
                sensordevice => "undefined",
                notification => "Telegram Bot notifications",
	            details      => "manual",
	            enrichment   => {
                    position => {
                        lng => $update<message><location><longitude>,
                        lat => $update<message><location><latitude>
                    },
                    title => $update<message><chat><username>
                }
            }
        }
    };

    my $response = $transport.send(:$data);

    if ($response<content>:exists) &&
        $response<content><result> &&
       ($response<msg>:exists) &&
        $response<msg> ~~ /^ '/api/ethelia/event/store fetch is successful' $/
    {
        $authnode.close_on_blockchain(:token(%validate<tx>), :waittx(False));

        return True;
    }

    bot_log(:message(sprintf("[FAILURE] failure while storing event %s", $response.gist)));

    return False;
}

sub decompress_row_data(Dossier :$dossier!, :$api!, Cool :$chatid, Str :$input!, Str :$lang) returns Bool {
    my $error;
    my $decoded  = [];

    if $input ~~ m:i/^ 0x<xdigit>+ $/ {
        try {
            my buf8 $buffer = NETETHEREUM.hex2buf($input);

#           $decoded = decompressToBlob($buffer).decode.split(q{|}, :skip-empty);
            $decoded = decompress_buffer(:$buffer).decode.split(q{|}, :skip-empty);

            CATCH {
                default {
                    $error = .message;

                    bot_log(:message(sprintf("[FAILURE] %s", $error)));
                }
            }
        }
    }
    else {
        $error = check_vocabulary(:$dossier, :$lang, :text('decompressusage'));
    }

    if $decoded && $decoded.elems == 7 {
        my $rowdata;

        for <id address code created payload topic compression>.kv -> $index, $key {
            $rowdata{$key} = $decoded[$index];
        }

        try {
            my $payload = from-json(MIME::Base64.new.decode-str($rowdata<payload>));

            $rowdata<payload> = $payload;

            CATCH {
                default {
                    $error = .massage;
                }
            }
        }

        if $rowdata.keys && $rowdata<payload>.keys {
            (my $json = to-json($rowdata)) ~~ s:i/\`//;

            $api.sendMessage(
                {
                    parse_mode => 'Markdown',
                    chat_id    => $chatid,
                    text       => sprintf("```javascript\n%s```", $json);
                }
            );

            return True;
        }
    }
    else {
        $error = check_vocabulary(:$dossier, :$lang, :text('decompressnull'));
    }

    $api.sendMessage({
        parse_mode => 'Markdown',
        chat_id    => $chatid,
        text       => sprintf(check_vocabulary(:$dossier, :$lang, :text('decompressfailure')), $error);
    });

    return False;
}

sub get_providers(Bool :$active = True) returns List {
    my $data = {
        credentials => {
            token => EVENTLISTTOKEN
        },
        method  => 'GET',
        route   => '/api/ethelia/event/search',
        payload => {
            search => {
                target => 1,
                value  => '*'
            }
        }
    };

    my $transport = Dossier::Transport::Request.new(:endpoint('https://pheix.org/api'));
    my $response  = $transport.send(:$data);

    if ($response<content>:exists) &&
       ($response<content><tparams>:exists) &&
       ($response<content><tparams><events>:exists) &&
        $response<content><tparams><events>.elems
    {
        my @providers;

        for $response<content><tparams><events>.values -> $event {
            my $payload;

            try {
                $payload = from-json(MIME::Base64.new.decode-str($event[3]));

                CATCH {
                    default { next; }
                }
            }

            my $created;

            if $event[2] ~~ m:i/ (\d+) ' ' (<[a .. z]>+) ' ' (\d+) ' ' (\d+\:\d+\:\d+) / {
                my @months = <Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec>;

                my $day  = +$0;
                my $mon  = ~$1;
                my $year = +$2;
                my $time = ~$3;

                $created = DateTime.new(sprintf("%04d-%02d-%02dT%s", $year, (@months.first($mon, :k) + 1), $day, $time), :timezone(0));

                next if $active && (time - $created.Instant.UInt > EVENTLIFETIME);
            }

            next unless
                $payload<sensorvalue> &&
                $payload<enrichment> &&
                $payload<enrichment><title> &&
                $payload<enrichment><position>;

            @providers.push({
                title       => sprintf("%s%s", $event[1] ~~ /^(iqair|aqicn)\-/ ?? q{} !! sprintf("%s ", CUPEMOJII), $payload<enrichment><title>.trim),
                sensorvalue => $payload<sensorvalue>,
                position    => $payload<enrichment><position>,
                created     => $created // $event[2],
                $created ?? %(activefor => time - $created.Instant.UInt) !! %(),
                ($payload<enrichment><transactionHash>:exists) ?? %(transactionHash => $payload<enrichment><transactionHash>) !! %()
            });
        }

        return @providers if @providers && @providers.elems;
    }

    return List.new;
}

sub providers(Dossier :$dossier!, :$api!, Cool :$chatid!, Str :$lang) returns Bool {
    my @providers = get_providers;

    if @providers && @providers.elems {
        my Str $formatted_text;

        for @providers.values -> $provider {
            my $link = ($provider<transactionHash>:exists) ?? sprintf("[%s](https://holesky.etherscan.io/tx/%s)", $provider<title>, $provider<transactionHash>) !! $provider<title>;

            $formatted_text ~= sprintf("• %s: *%d*\n", $link, $provider<sensorvalue>);
        }

        $api.sendMessage({
            parse_mode => 'Markdown',
            link_preview_options => {
                is_disabled => True
            },
            chat_id    => $chatid,
            text       => $formatted_text,
        });

        return True;
    }

    $api.sendMessage({
        chat_id => $chatid,
        text    => check_vocabulary(:$dossier, :$lang, :text('providersfailure')),
    });

    return False;
}

sub transaction_log(Dossier :$dossier!, :$api!, Cool :$chatid!, Str :$lang) returns Bool {
    my @logfiles = $*CWD.dir.grep({$_.basename ~~ /^ 'ethelia-stations.raku-sgn-' /});

    if @logfiles && @logfiles.elems {
        my @transaction_log;

        for @logfiles.sort.reverse[0..(MAXFILES - 1)].values -> $logfile {
            next unless $logfile;

            @transaction_log.push(|$logfile.lines.reverse.map({ $_.split(q{|}, :skip-empty).map({ $_.trim }) }));
        }

        if @transaction_log && @transaction_log.elems {
            my $formatted_text;

            my $maxpos = 64;

            while ($maxpos > 0 && !$formatted_text) {
                for @transaction_log[0 .. $maxpos].values -> $trx {
                    if $trx && $trx.elems && $trx[4] ~~ / '0x' <xdigit> ** 64 / {
                        my $date = $trx[0];

                        $date ~~ s/\.\d+$//;
                        $date ~~ s/\s/T/;

                        my $link = sprintf("[%s…](https://holesky.etherscan.io/tx/%s)", $trx[4].substr(0, 12), $trx[4]);

                        $formatted_text ~= sprintf("• %s: %s\n", DateTime.new($date, formatter => { sprintf "%04d/%02d/%02d %s", .year, .month, .day, .hh-mm-ss }).in-timezone($*TZ), $link);
                    }
                }

                $formatted_text = Empty unless $formatted_text.chars < 4096;
                $maxpos--;
            }

            if $formatted_text && $formatted_text.chars {
                $api.sendMessage({
                    parse_mode => 'Markdown',
                    link_preview_options => {
                        is_disabled => True
                    },
                    chat_id    => $chatid,
                    text       => $formatted_text,
                });

                return True;
            }
        }
    }

    $api.sendMessage({
        chat_id => $chatid,
        text    => check_vocabulary(:$dossier, :$lang, :text('trxlogfailure')),
    });

    return False;
}

sub bot_utils(:$api!, Cool :$chatid!, Str :$command!) returns Hash {
    my $ret = {
        time   => DateTime.new(now, timezone => $*TZ, formatter => { sprintf "%04d/%02d/%02d %02d:%02d:%02d", .year, .month, .day, .hour, .minute, .second }),
        status => False,
    }

    if $ret{$command}:exists {
        $api.sendMessage({
            chat_id => $chatid,
            text    => $ret{$command},
        });

        $ret<status> = True;
    }

    return $ret;
}

sub check_vocabulary(Dossier :$dossier!, Str :$text!, Str :$lang) returns Str {
    my $config = $dossier.load_config;

    if $config<vocabulary> && ($config<vocabulary>{$text} || $config<vocabulary><system_messages>{$text}) {
        my $vocabulary = $config<vocabulary>{$text} // $config<vocabulary><system_messages>{$text};

        if $vocabulary{$lang} {
            return ~$vocabulary{$lang};
        }
        elsif $vocabulary{DEFAULTLANG} {
            return ~$vocabulary{DEFAULTLANG};
        }
        else {
            ;
        }
    }

    return $text
}

sub blockchainsync_log(:$api!, Cool :$chatid!, Dossier :$dossier, Str :$lang) returns Bool {
    my @logfiles = $*CWD.dir.grep({$_.basename ~~ /^ 'update-dbs.bash_' /});

    if @logfiles && @logfiles.elems {
        my @sync_log;

        for @logfiles.sort.reverse[0..(MAXFILES - 1)].values -> $logfile {
            next unless $logfile;

            @sync_log.push(|$logfile.lines.reverse.grep({$_ !~~ / '0x' <xdigit> ** 64 / }).map({ $_.trim  }));
        }

        if @sync_log && @sync_log.elems {
            my $formatted_text;

            my $maxpos = @sync_log.elems > 64 ?? 64 !! @sync_log.elems;

            while ($maxpos > 0 && !$formatted_text) {
                for @sync_log[0 .. ($maxpos - 1)].values -> $logrec {
                    $formatted_text ~= sprintf("%s\n", $logrec);
                }

                $formatted_text = Empty unless $formatted_text.chars < 4096;
                $maxpos--;
            }

            if $formatted_text && $formatted_text.chars {
                $formatted_text ~~ s:g/ '`' / '_' /;

                $api.sendMessage({
                    parse_mode => 'Markdown',
                    chat_id    => $chatid,
                    text       => sprintf("```bash\n%s```", $formatted_text),
                });

                return True;
            }
        }
    }

    $api.sendMessage({
        chat_id => $chatid,
        text    => check_vocabulary(:$dossier, :$lang, :text('synclogfailure')),
    });

    return False;
}

sub get_balance(:$api!, Cool :$chatid!, Dossier :$dossier, Str :$lang) returns Bool {
    my $sepolia = Pheix::Model::Database::Access.new(
        :table('holesky_local_storage'),
        :fields([]),
        :jsonobj(init_pheix_config(:path(sprintf("%s/conf", PHEIXPATH))))
    );

    if $sepolia && $sepolia.dbswitch == 1 {
        my $eth     = $sepolia.chainobj.ethobj;
        my $balance = $eth.wei2ether($eth.eth_getBalance($sepolia.chainobj.ethacc, 'latest'));

        $api.sendMessage({
            parse_mode => 'Markdown',
            chat_id    => $chatid,
            text       => sprintf(check_vocabulary(:$dossier, :$lang, :text('balanceok')), $balance),
        });

        return True;
    }

    $api.sendMessage({
        chat_id => $chatid,
        text    => check_vocabulary(:$dossier, :$lang, :text('balancefailure')),
    });

    return False;
}

sub geolocation_completed(Cool :$chatid!, Dossier :$dossier, Str :$lang = DEFAULTLANG, Hash :$update = {}) returns Str {
    $dossier.activate_dossier(:$chatid, :graphname(GRAPHNAME));

    my $chatdossier = $dossier.get_dossier(:$chatid);

    $dossier.activate_dossier(:$chatid, :graphname('geolocation'));

    my $geolocationdossier = $dossier.get_dossier(:$chatid);
    my $sensorvalue        = $geolocationdossier<nodes><1><answer>.UInt;

    if $sensorvalue && $sensorvalue > 0 && $sensorvalue < 500 {
        my $resulttext  = check_vocabulary(:$dossier, :$lang, :text('pm25failed'));

        if  is_registration_completed(:$chatdossier) &&
            store_pm25_sensorvalue(:$dossier, :$update, :$sensorvalue)
        {
            $resulttext = sprintf(check_vocabulary(:$dossier, :$lang, :text('pm25saved')), $sensorvalue);

            bot_log(:message(
                sprintf(
                    "successfully saved PM2.5 sensor value %d from %s",
                    $sensorvalue, ($update<message><chat><username> // $update<message><chat><first_name> // ANONYMOUS)
                )
            ));
        }

        $dossier.activate_dossier(:$chatid, :graphname(GRAPHNAME));

        return $resulttext;
    }

    $dossier.activate_dossier(:$chatid, :graphname(GRAPHNAME));

    return check_vocabulary(:$dossier, :$lang, :text('nopm25value'));
}

sub show_help(:$api!, :$chatid!, Dossier :$dossier, Str :$lang = DEFAULTLANG) returns Bool {
    $api.sendMessage({
        parse_mode => 'Markdown',
        link_preview_options => {
            is_disabled => True
        },
        chat_id    => $chatid,
        text       => check_vocabulary(:$dossier, :$lang, :text('helpmessage')),
        reply_markup => {remove_keyboard => True}
    });

    return True;
}

sub geth_log(Dossier :$dossier!, :$api!, Cool :$chatid!, Str :$lang) returns Bool {
    my $logfile = $*CWD.dir.grep({$_.basename eq 'geth-holesky-node.log'}).head;

    if $logfile {
        my $length  = 4096;
        my $logdata = $logfile.slurp.substr(0, 4096 - 1);

        if $logdata && $logdata.chars < 4096 {
            $api.sendMessage({
                parse_mode => 'Markdown',
                chat_id    => $chatid,
                text       => sprintf("```%s\n%s```", 'geth-node-logs', $logdata),
            });

            return True;
        }
    }

    $api.sendMessage({
        chat_id => $chatid,
        text    => check_vocabulary(:$dossier, :$lang, :text('gethlogfailure')),
    });

    return False;
}

sub decompress_buffer(buf8 :$buffer) returns buf8 {
    my int32 $rc;

    my buf8 $decoded_buf .= new;
    my uint32 $buf_length = $buffer.elems * 2;

    my $return_codes_map = {
        '-9' => 'BZ_CONFIG_ERROR',
        '-8' => 'BZ_OUTBUFF_FULL',
        '-7' => 'BZ_UNEXPECTED_EOF',
        '-6' => 'BZ_IO_ERROR',
        '-5' => 'BZ_DATA_ERROR_MAGIC',
        '-4' => 'BZ_DATA_ERROR',
        '-3' => 'BZ_MEM_ERROR',
        '-2' => 'BZ_PARAM_ERROR',
        '-1' => 'BZ_SEQUENCE_ERROR',
        '0'  => 'BZ_OK',
        '1'  => 'BZ_RUN_OK',
        '2'  => 'BZ_FLUSH_OK',
        '3'  => 'BZ_FINISH_OK',
        '4'  => 'BZ_STREAM_END',
    };

    while True {
        my Int $len = $buf_length;
        $decoded_buf[$len] = 0;

        try {
            $rc = BZ2_bzBuffToBuffDecompress($decoded_buf, $buf_length, $buffer, $buffer.elems, 0, 0);

            CATCH {
                default {
                    $rc = -9_999_999;
                }
            }
        }

        if $rc == BZ_OK {
            return $decoded_buf.subbuf(0, $buf_length);
        } elsif $rc == BZ_OUTBUFF_FULL {
            $buf_length *= 2;
            next;
        } else {
            X::AdHoc.new(:payload(sprintf("failure during BZip2 decompression: %s", $return_codes_map{~$rc}))).throw;
        }
    }
}
