use v6.d;
use Test;

use Dossier;
use Dossier::Transport::MockSock;

my $interactive = (%*ENV<DOSSINTERACTIVE>.defined && %*ENV<DOSSINTERACTIVE> == 1) ?? True !! False;

plan 7;

use-ok 'Dossier', 'Dossier is used ok';

my $dossier = Dossier.new(test => ($interactive ^^ 1).Bool, cpath => './x/conf.yaml');

ok $dossier, 'Dossier object';
is $dossier.get_name, 'dossier', 'get_name method works';
ok $dossier.mockedsock ~~ Dossier::Transport::MockSock, 'mocked socket exists';
ok $dossier.mockedsock.^find_method('print'), 'can print to mocked socket';
ok $dossier.mockedsock.^find_method('close'), 'can close mocked socket';
ok !$dossier.mockedsock.^find_method('ioctl'), 'no ioctl can be applied to mocked socket';

if $interactive {
    $dossier.debug;
}
else {
    diag('skip interactive mode, set DOSSINTERACTIVE=1 to switch it on');
}

done-testing;
